\ LED_BUILTIN is PB5

$25 constant PORTB
$24 constant DDRB

: led-init %00100000 DDRB c! ; \ configure PB5 as an output pin
: led-on %00100000 PORTB c! ; \ drive PB5 to high
: led-off %00000000 PORTB c! ; \ drive PB5 to low

: e:blink
  led-init
  begin led-on 1000 ms led-off 1000 ms again
;
