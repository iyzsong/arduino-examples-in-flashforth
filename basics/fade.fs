\ D9 is PB1, OC1A

$24 constant DDRB
$25 constant PORTB
$80 constant TCCR1A \ COM1A1, COM1A0, COM1B1, COM1B0, -, -, WGM11, WGM10
$81 constant TCCR1B \ ICNC1, ICES1, -, WGM13, WGM12, CS12, CS11, CS10
$88 constant OCR1A
%00000010 constant PIN1
variable fade-amount
5 fade-amount !

: >= < 0= ;
: <= > 0= ;

: brightness+
  fade-amount @ +
  dup 0 <= over 255 >= or if fade-amount @ negate fade-amount ! then
;

: e:fade
  \ configure PB1 as an output pin
  PIN1 DDRB c!

  \ Fast PWM, 8bit, prescaler=1024
  %10000001 TCCR1A c!
  %00001101 TCCR1B c!

  0 begin dup OCR1A ! brightness+ 30 ms again
;
