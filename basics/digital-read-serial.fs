$29 constant PIND
$2a constant DDRD

: PD2 PIND c@ %00000100 and ;

: e:digital-read
  \ configure PD2 as an input pin
  %11111011 DDRD c!
  begin cr PD2 . 200 ms again
;
