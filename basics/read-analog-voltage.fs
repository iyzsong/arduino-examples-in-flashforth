\ A0 is PC0, ADC0

$7c constant ADMUX
$7a constant ADCSRA
$79 constant ADCH
$78 constant ADCL

: adc0-init
  %01000000 ADMUX c!
  %10000111 ADCSRA c!
;

: adc0-read ( -- n )
  ADCSRA c@ %01000000 or ADCSRA c!
  begin ADCSRA c@ %01000000 and 0= until
  ADCL c@
  ADCH c@ 8 lshift or
;

: voltage ( n -- n n )
  50 * 1023 u/mod swap drop
  10 u/mod
;

: e:analog-readv
  adc0-init
  begin
    adc0-read voltage
    48 + emit 46 emit 48 + emit cr
  again
;
